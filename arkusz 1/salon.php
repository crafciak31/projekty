<!-- https://egzamin-e14.blogspot.com/2017/10/arkusz-e14-04-1706.html -->
<html>
     <head>
          <title>Salon pielęgnacji</title>
          <meta charset="UTF-8">
          <link rel="stylesheet" type="text/css" href="salon.css">
          <?php $polacz=mysqli_connect("localhost", "root", "", "salon")?>
     </head>
     <body>
          <div class="baner">
               <h1>SALON PIELĘGNACJI PSÓW I KOTÓW</h1>
          </div>
          <div class="lewy">
               <h3>SALON ZAPRASZA W DNIACH</h3>
               <ul>
                    <li>Poniedziałek, 12:00 - 18:00</li>
                    <li>Wtorek, 12:00 - 18:00</li>
               </ul>
               <img src="pies-mini.jpeg" alt="">
               <p>Umów się telefonicznie na wizytę lub po prostu przyjdź!</p>
          </div>
          <div class="srodkowy">
               <h3>PRZYPOMNIENIE O NASTĘPNEJ WIZYCIE</h3>
               <?php
                    $wizyta = mysqli_query($polacz, "SELECT imie, rodzaj, nastepna_wizyta, telefon FROM zwierzeta WHERE nastepna_wizyta != 0");
                    
                    while($w = mysqli_fetch_array($wizyta)){
                         if($w[1]==1){
                              echo "Pies: ".$w[0]."<br>";
                         }
                         if($w[1]==2){
                              echo "Kot: ".$w[0]."<br>";
                         }
                         echo "Data następnej wizyty: ".$w[2].", telefon właściciela: ".$w[3].". <br>";
                    }
               ?>
          </div>
          <div class="prawy">
               <h3>USŁUGI</h3>
               <?php 
               $sql="SELECT nazwa, cena FROM uslugi";
               $rezultat=mysqli_query($polacz,$sql);
               while($r=mysqli_fetch_array($rezultat)){
                    echo $r["nazwa"].", ";
                    echo $r["cena"]."<br>";
               }
               ?>
          </div>
     </body>
</html>
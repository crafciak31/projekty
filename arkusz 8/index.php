<html>
    <head>
        <meta charset="UTF-8">
        <title>Sklep papierniczy</title>
        <link rel="stylesheet" href="styl.css">
        <?php $connect=mysqli_connect('localhost', 'root', '', 'sklep') ?>
    </head>
    <body>
        <div class="baner">
            <h1>W naszym sklepie internetowym kupisz najtaniej</h1>
        </div>
        <div class="lewy">
            <h3>Promocja 15% obejmuje artykuły:</h3>
            <ul>
                <?php 
                 
                $promocja = mysqli_query($connect, 'SELECT nazwa FROM towary WHERE promocja = 1');

                while($p=mysqli_fetch_array($promocja)){
                    echo "<li>".$p['nazwa']."</li>";
                }
                
                ?>
            </ul>
        </div>
        <div class="srodkowy">
            <h3>Cena wybranego artykułu w promocji</h3>
            <form method="post">
                <select name="przedmiot">
                <?php 
                 
                 $promocja = mysqli_query($connect, 'SELECT nazwa FROM towary WHERE promocja = 1');
 
                 while($p=mysqli_fetch_array($promocja)){
                     echo "<option value='".$p['nazwa']."' >".$p['nazwa']."</option>";
                 }
                 
                 ?>
                </select>
                <input type="submit" name="wybierz" value="WYBIERZ">
            </form>
            <?php
            if(isset($_POST['wybierz'])){
                $cena = mysqli_fetch_array(mysqli_query($connect, "SELECT cena FROM towary WHERE nazwa='".$_POST['przedmiot']."'"));
                $c = $cena[0]*0.85;
                echo "Cena po promocji wynosi ".number_format($c, 2)."zł";
            }
            ?>
        </div>
        <div class="prawy">
            <h3>Kontakt</h3>
            <p>telefon: 123123123 <br>e-mail: <a href="mailto:bok@sklep.pl"> bok@sklep.pl</a></p>
            <img src="promocja2.png" alt="promocja">
        </div>
        <div class="stopka">
            <h4>Autor strony Paweł Mazur</h4>
        </div>
    </body>
</html>
-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 27 Wrz 2019, 10:31
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `typowa baza danych`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `id` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`id`, `nazwa`, `adres`, `telefon`, `NIP`) VALUES
(1, 'UPS', 'Brzozowska 12', '123456789', '111222333444'),
(2, 'DHL', 'Sosnowska 13', '987654321', '444333222111'),
(3, 'FedEx', 'Drzewowska 14', '918273645', '999888777666');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `id` int(10) NOT NULL,
  `alias` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`id`, `alias`, `ulica`, `miasto`, `kod`, `kraj`) VALUES
(1, 'Konon & Major Sp. z. o. o.', 'Zbigniewa 15', 'Białystok', '0000000001', 'Podlasie'),
(2, 'Gazownia A. H.', 'Achtung 16', 'Berlin', '1965000013', 'Niemcy'),
(3, 'Kraina Denaturatu', 'Makarova 17', 'Moskwa', '1212121212', 'Rosja'),
(4, 'Tusk Rodzinny Interes', 'Donalda 18', 'Rzeszów', '4204204021', 'Polska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `specyfikacja` text COLLATE utf8_polish_ci DEFAULT NULL,
  `kod-produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`id`, `nazwa`, `specyfikacja`, `kod-produktu`, `cena`) VALUES
(1, 'Cichobiegi \"Szelest\"', 'Laczki o niezwykłych właściwościach magicznych', '111111', 59),
(2, 'Paróweczki Podlaskie', 'Gotowane przez największych mistrzów obierania pomarańczy', '222222', 49),
(3, 'Papier ścierny', 'Idealny do podtarcia się w czwartek', '333333', 29),
(4, 'Muszynianka', 'Murzyn gratis', '444444', 9),
(5, 'Gruz', 'Wydobywany z wnętrza Rumuni, nagrodzony przez instytut matki i cyganka', '555555', 89),
(6, 'Taczka z drewna', 'Tu nie ma zaskoczenia, posiada jedno koło', '666666', 69),
(7, 'BMX', '6 tysięcy koni mechanicznych, Różowy kolor, turbo, nitro, 2 złote pedały i jeden czarny, 3 złote w koszyku', '777777', 129),
(8, 'Wiaderko', 'Dziurawe', '888888', 1),
(9, 'Kotlet z psa', 'Trzeciej kategorii, przemielone razem z budą', '999999', 19),
(10, 'Pan Tadeusz', 'Autor: Ksiądz Robak (Prywatnie: Jacek Soplica)', '101010', 999);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `id` int(10) NOT NULL,
  `idproduktu` int(10) NOT NULL,
  `idmagazynu` int(10) NOT NULL,
  `ilość` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`id`, `idproduktu`, `idmagazynu`, `ilość`) VALUES
(1, 1, 1, 420),
(2, 2, 2, 320),
(3, 3, 3, 220),
(4, 4, 4, 120),
(5, 5, 1, 520),
(6, 6, 2, 620),
(7, 7, 3, 123),
(8, 8, 4, 654),
(9, 9, 1, 12),
(10, 10, 2, 66);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

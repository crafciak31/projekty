<!doctype html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <title>Biblioteka publiczna</title>
    <link rel="stylesheet" href="style.css">
    <?php $connect= mysqli_connect('localhost', 'root', '', 'biblioteka');?>
</head>

<body>
    <div class="baner">
        <h2>Miejska Biblioteka Publiczna w Książkowicach</h2>
    </div>
    <div class="lewy">
        <h2>Dodaj czytelnika</h2>
        <form action="" method="post">
            imię: <input type="text" name="imie" id="imie"><br>
            nazwisko: <input type="text" name="nazwisko" id="nazwisko"><br>
            rok urodzenia: <input type="number" name="rok" id="rok"><br>
            <input type="submit" name="dodaj" value="DODAJ"> 
        </form>
        <?php 
        if(isset($_POST['dodaj'])){
            echo "Czytelnik: ".$_POST['imie']." ".$_POST['nazwisko']." został dodany do bazy danych";
            $kod = strtolower(substr($_POST['imie'], 0, 2).substr($_POST['rok'], -2).substr($_POST['nazwisko'], 0, 2));
            mysqli_query($connect, "INSERT INTO `czytelnicy` (`id`, `imie`, `nazwisko`, `kod`) VALUES (NULL, '".$_POST['imie']."', '".$_POST['nazwisko']."', '".$kod."')");
        }
        ?>
    </div> 
    <div class="srodkowy">
        <img src="biblioteka.png" alt="biblioteka">
        <h4>ul. Czytelnicza 25<br> 
        12-120 Książkowice<br> 
        tel.: 123123123<br> 
        e-mail: biuro@bib.pl</h4>
    </div>
    <div class="prawy">
        <h3>Nasi czytelnicy:</h3>
        <ul>
        <?php 
        $czytelnicy = mysqli_query($connect, "SELECT imie, nazwisko FROM czytelnicy");
        while($czytelnik=mysqli_fetch_array($czytelnicy)){
            echo "<li>".$czytelnik['imie']." ".$czytelnik['nazwisko']."</li>";
        }
        mysqli_close($connect);
        ?>
        </ul>
    </div>
    <div class="stopa">
        <p>Projekt witryny: Paweł Mazur</p>
    </div>
</body>

</html>